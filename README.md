# hse-logger

A OOP based logging system inspired by the Phython logging module.

## :bulb: Documentation

The HSE Dokuwiki holds more information:

* https://dokuwiki.hampel-soft.com/code/open-source/hse-logger

### :question: FAQ
See our [FAQ](https://dokuwiki.hampel-soft.com/code/open-source/hse-logger/faq) for comments on updating versions amongst other things.

Your use case is not supported? Check out out [Commercial Plugins](https://dokuwiki.hampel-soft.com/code/commercial/hse-logger_extensions) and please get in touch at (office@hampel-soft.com). 

## :rocket: Installation

Download the latest VI Package at https://www.vipm.io/package/hse_lib_hse_logger and install it globally
with the [VI Package Manager](https://www.vipm.io/download/). 

If you want to use the HSE-Logger only in a project scope use the Source Distribution.
Then you can copy the `hse-logger.lvlib` into your project.

The latest release version can also be found at 
https://dokuwiki.hampel-soft.com/code/open-source/hse-logger/releases

### Configuration 

No configuration needed.

### :wrench: LabVIEW 2016

The VIs are maintained in LabVIEW 2016.

## :bulb: Usage

See example files in folder `Examples`.

## :busts_in_silhouette: Contributing 

We welcome every and any contribution. On our Dokuwiki, we compiled detailed information on 
[how to contribute](https://dokuwiki.hampel-soft.com/processes/collaboration). 
Please get in touch at (office@hampel-soft.com) for any questions.

## :beers: Credits

* Manuel Sebald
* Joerg Hampel
* John Medland

## :page_facing_up: License 

This project is licensed under a modified BSD License - see the [LICENSE](LICENSE) file for details